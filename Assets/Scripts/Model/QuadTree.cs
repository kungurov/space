﻿using System.Collections.Generic;
using UnityEngine;

public class QuadTree<T> where T : class, ISpaceObject
{
	public Rect Bounds { get; private set; }
	public QuadTree<T>[] Nodes { get; private set; }
	private readonly int _itemsPerQuad;
	private readonly List<T> _items;
	public bool Generated;
		
	public QuadTree(int size, Rect bounds)
	{
		Bounds = bounds;
		_itemsPerQuad = size;
		Nodes = new QuadTree<T>[4];
		_items = new List<T>(size);
		Generated = false;
	}

	public void Insert(T items)
	{
		if (Nodes[0] != null)
		{
			var node = GetCellToInsertObject(items.GetPosition());
			if (node != null)
			{
				Nodes[node.Value].Insert(items);
			}
			return;
		}
		_items.Add(items);
		if (_items.Count > _itemsPerQuad)
		{
			if (Nodes[0] == null)
			{
				Split();
			}
			for (var i = _items.Count - 1; i >= 0; --i)
			{
				T storedObj = _items[i];
				int? iCell = GetCellToInsertObject(storedObj.GetPosition());
				if (iCell != null)
				{
					Nodes[iCell.Value].Insert(storedObj);
				}
				_items.RemoveAt(i);
				i--;
			}
		}
	}

	public void Split()
	{
		var width = (Bounds.width / 2f);
		var height = (Bounds.height / 2f);
		var x = Bounds.x;
		var y = Bounds.y;
		if (Nodes[0] == null)
			Nodes[0] = new QuadTree<T>(_itemsPerQuad, new Rect(x + width, y, width, height));
		if (Nodes[1] == null)
			Nodes[1] = new QuadTree<T>(_itemsPerQuad, new Rect(x, y, width, height));
		if (Nodes[2] == null)
			Nodes[2] = new QuadTree<T>(_itemsPerQuad, new Rect(x, y + height, width, height));
		if (Nodes[3] == null)
			Nodes[3] = new QuadTree<T>(_itemsPerQuad, new Rect(x + width, y + height, width, height));
	}

	public void Remove(T items)
	{
		if (ContainsPoint(items.GetPosition()))
		{
			_items.Remove(items);
			if (Nodes[0] != null)
			{
				for (int i = 0; i < 4; i++)
				{
					Nodes[i].Remove(items);
				}
			}
		}
	}

	public void GetObjectsInArea(Rect area, ref HashSet<T> selectedItems)
	{
		if (Bounds.Overlaps(area))
		{
			for (var i = 0; i < _items.Count; ++i)
			{
				if (area.Contains(_items[i].GetPosition()))
				{
					selectedItems.Add(_items[i]);
				}
			}
			if (Nodes[0] != null)
			{
				for (var i = 0; i < 4; i++)
				{
					Nodes[i].GetObjectsInArea(area, ref selectedItems);
				}
			}
		}
	}

	public void Clear()
	{
		_items.Clear();

		for (var i = 0; i < Nodes.Length; i++)
		{
			if (Nodes[i] != null)
			{
				Nodes[i].Clear();
				Nodes[i] = null;
			}
		}
	}

	public bool ContainsPoint(Vector2 point)
	{
		return Bounds.Contains(point);
	}

	private int? GetCellToInsertObject(Vector2 point)
	{
		for (var i = 0; i < 4; i++)
		{
			if (Nodes[i].ContainsPoint(point))
			{
				return i;
			}
		}
		return null;
	}

/*	public void DrawDebug()
	{
		Gizmos.color = Color.white;
		Gizmos.DrawLine(new Vector3(Bounds.x, Bounds.y, 0), new Vector3(Bounds.x, Bounds.y + Bounds.height, 0));
		Gizmos.DrawLine(new Vector3(Bounds.x, Bounds.y, 0), new Vector3(Bounds.x + Bounds.width, Bounds.y, 0));
		Gizmos.DrawLine(new Vector3(Bounds.x + Bounds.width, Bounds.y, 0), new Vector3(Bounds.x + Bounds.width, Bounds.y + Bounds.height, 0));
		Gizmos.DrawLine(new Vector3(Bounds.x, Bounds.y + Bounds.height, 0), new Vector3(Bounds.x + Bounds.width, Bounds.y + Bounds.height, 0));
		if (Nodes[0] != null)
		{
			for (int i = 0; i < Nodes.Length; i++)
			{
				if (Nodes[i] != null)
				{
					Nodes[i].DrawDebug();
				}
			}
		}
	}
	*/		
}