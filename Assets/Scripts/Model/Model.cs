﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Model
{
    public event Action PlanetsChanged = delegate { };
        
    public readonly SpaceshipModel Spaceship;
    public QuadTree<PlanetModel> Planets;    
    public readonly List<PlanetModel> VisiblePlanets;
    private HashSet<PlanetModel> _selectedPlanets;
    private readonly HashSet<PlanetModel> _addedPlanets;
    
    public Model()
    {
        var area = new Rect(new Vector2(0, 0), new Vector2(Settings.START_SPACE_AREA, Settings.START_SPACE_AREA));
        Spaceship = new SpaceshipModel(GetPosition(new Vector2Int(Settings.START_SPACE_AREA / 2, Settings.START_SPACE_AREA / 2)), Settings.DEFAULT_VISIBLE_SIZE, GetRating());
        Planets = new QuadTree<PlanetModel>(Settings.OBJECTS_PER_QUAD, area);
        _selectedPlanets = new HashSet<PlanetModel>();
        _addedPlanets = new HashSet<PlanetModel>();
        VisiblePlanets = new List<PlanetModel>();
        
        GeneratePlanets(Planets);
        Move(Vector2.zero);
    }

    private void GeneratePlanets()
    {
        Planets.Generated = true;
        foreach (var quad in Planets.Nodes)
        {
            if (!quad.Generated)
            {
                GeneratePlanets(quad);
            }
        }
    }

    private void GeneratePlanets(QuadTree<PlanetModel> quad)
    {
        quad.Generated = true;
        for (var i = 0; i < quad.Bounds.width; ++i)
        {
            for (var j = 0; j < quad.Bounds.height; ++j)
            {
                if (UnityEngine.Random.value < Settings.PLANET_CHANCE)
                {
                    var position = new Vector2(quad.Bounds.x, quad.Bounds.y) + GetPosition(new Vector2Int(i, j));
                    var bounds = new Rect(position - Settings.PLANET_SIZE / 2f, Settings.PLANET_SIZE);
                    var rating = GetRating();
                    var diff = Mathf.Abs(Spaceship.GetRating() - rating);
                    var planet = new PlanetModel(position, bounds, rating, diff);
                    Planets.Insert(planet);
                }
            }
        }
    }

    private int GetRating()
    {
        return UnityEngine.Random.Range(Settings.RATING_MIN, Settings.RATING_MAX);
    }

    private Vector2 GetPosition(Vector2Int position)
    {
         return position + Settings.CELL_SIZE / 2f;   
    }

    public void ZoomIn()
    {
        Spaceship.VisibleSize -= 1;
        Move(Vector2.zero);
    }

    public void ZoomOut()
    {
        Spaceship.VisibleSize += 1;        
        Move(Vector2.zero);
    }

    public void MoveUp()
    {
        Move(new Vector2(0, Settings.CELL_SIZE.y));
    }

    public void MoveDown()
    {
        Move(new Vector2(0, -Settings.CELL_SIZE.y));
    }
    
    public void MoveRigth()
    {
        Move(new Vector2(-Settings.CELL_SIZE.x, 0));
    }
    
    public void MoveLeft()
    {
        Move(new Vector2(Settings.CELL_SIZE.x, 0));
    }

    private void AddVisiblePlanets(PlanetModel planet)
    {
        int max = (VisiblePlanets.Count > 0) ? VisiblePlanets[VisiblePlanets.Count - 1].GetRatingDiff() : Settings.RATING_MAX;            
        bool changed = false;
        if (VisiblePlanets.Count < Settings.OBJECTS_SAMPLE)
        {
            VisiblePlanets.Add(planet);
            changed = true;
        }
        else if (planet.GetRatingDiff() < max)
        {
            VisiblePlanets[VisiblePlanets.Count - 1] = planet;
            changed = true;
        }
        if (changed)
        {
            VisiblePlanets.Sort((x, y) => x.GetRatingDiff().CompareTo(y.GetRatingDiff()));
            max = VisiblePlanets[VisiblePlanets.Count - 1].GetRatingDiff();
        }
    }

    private void Move(Vector2 move)
    {
        Spaceship.Position += move;
        UpdateSpace();
        UpdateVisiblePlanets();

    }

    private void UpdateSpace()
    {
        var rightUpPoint = new Vector2(Spaceship.GetPosition().x + Spaceship.VisibleSize, Spaceship.GetPosition().y + Spaceship.VisibleSize);
        var rightDownPoint = new Vector2(Spaceship.GetPosition().x + Spaceship.VisibleSize, Spaceship.GetPosition().y - Spaceship.VisibleSize);
        var leftUpPoint = new Vector2(Spaceship.GetPosition().x - Spaceship.VisibleSize, Spaceship.GetPosition().y + Spaceship.VisibleSize);
        var leftDownPoint = new Vector2(Spaceship.GetPosition().x - Spaceship.VisibleSize, Spaceship.GetPosition().y - Spaceship.VisibleSize);
        
        if ((!Planets.ContainsPoint(rightUpPoint) && !Planets.ContainsPoint(rightDownPoint)) ||
            (!Planets.ContainsPoint(leftUpPoint) && !Planets.ContainsPoint(rightUpPoint)))
        {
            CreateRootQuad(1, new Vector2(Planets.Bounds.x, Planets.Bounds.y));
        }
        if ((!Planets.ContainsPoint(leftUpPoint) && !Planets.ContainsPoint(leftDownPoint)) ||
            (!Planets.ContainsPoint(leftDownPoint) && !Planets.ContainsPoint(rightDownPoint)))
        {
            CreateRootQuad(3, new Vector2(Planets.Bounds.x - Planets.Bounds.width, Planets.Bounds.y - Planets.Bounds.height));
        }
    }

    private void CreateRootQuad(int node, Vector2 position)
    {
        var area = new Rect(position, new Vector2(Planets.Bounds.width * 2, Planets.Bounds.height * 2));
        var quad = new QuadTree<PlanetModel>(Settings.OBJECTS_PER_QUAD, area);
        quad.Nodes[node] = Planets;
        quad.Split();
        Planets = quad;
        GeneratePlanets();
    }

    private void UpdateVisiblePlanets()
    {        
        HashSet<PlanetModel> selection = new HashSet<PlanetModel>();
        Planets.GetObjectsInArea(Spaceship.GetBounds(), ref selection); 
        foreach (var item in selection)
        {
            if (!_selectedPlanets.Contains(item))
            {
                _addedPlanets.Add(item);
            }
            else
            {
                _selectedPlanets.Remove(item);
            }
        }
        foreach (var item in _selectedPlanets)
        {
            VisiblePlanets.Remove(item);
        }
        foreach (var item in _addedPlanets)
        {
            AddVisiblePlanets(item);
        }
        _selectedPlanets = selection;
        _addedPlanets.Clear();
        PlanetsChanged();        
    }

    public void Update()
    {
    }

    public void Destroy()
    {
        Planets.Clear();
        _selectedPlanets.Clear();
        _addedPlanets.Clear();
    }   
}
