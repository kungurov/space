﻿using UnityEngine;

public interface ISpaceObject
{
    Rect GetBounds();
    int GetRating();
    Vector2 GetPosition();
}
