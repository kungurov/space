﻿using UnityEngine;

public class PlanetModel : ISpaceObject
{
    private readonly Rect _bounds;
    private readonly Vector2 _position;
    private readonly int _rating;
    private readonly int _ratingDiff;
        
    public PlanetModel(Vector2 position, Rect bounds, int rating, int diff)
    {
        _bounds = bounds;
        _rating = rating;
        _ratingDiff = diff;
        _position = position;
    }
    
    public Rect GetBounds()
    {
        return _bounds;
    }

    public Vector2 GetPosition()
    {
        return _position;
    }
    
    public int GetRating()
    {
        return _rating;
    }
    
    public int GetRatingDiff()
    {
        return _ratingDiff;
    }
}
