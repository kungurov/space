﻿using System;
using UnityEngine;

public class SpaceshipModel : ISpaceObject
{
    public event Action PositionChanged = delegate { };
    public event Action VisibleSizeChanged = delegate { };

    private readonly int _rating;
    
    private Vector2 _position;
    public Vector2 Position
    {
        get { return _position; }
        set
        {
            if (_position != value)
            {
                _position = value;
                PositionChanged();
            }
        }
    }

    private int _visibleSize;
    public int VisibleSize
    {
        get { return _visibleSize; }
        set
        {
            if (_visibleSize != value)
            {
                _visibleSize = value;
                VisibleSizeChanged();
            }
        }
    }

    public SpaceshipModel(Vector2 position, int size, int rating)
    {
        Position = position;
        VisibleSize = size;
        _rating = rating;
    }
    
    public Rect GetBounds()
    {
        var size = new Vector2(VisibleSize, VisibleSize);
        var left = new Vector2(Position.x, Position.y) - size / 2.0f;
        return new Rect(left, size);
    }
    
    public int GetRating()
    {
        return _rating;
    }

    public Vector2 GetPosition()
    {
        return _position;
    }
}
