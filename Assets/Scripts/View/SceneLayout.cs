﻿using UnityEngine;
using UnityEngine.UI;

public class SceneLayout : MonoBehaviour
{    
    public Button ButtonZoomIn;
    public Button ButtonZoomOut;
    public Button ButtonMoveUp;
    public Button ButtonMoveDown;
    public Button ButtonMoveRight;
    public Button ButtonMoveLeft;
    public GameObject PlanetsController;
    public Text TextVisibleSize;
}
