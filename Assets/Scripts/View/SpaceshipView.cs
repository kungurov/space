﻿public class SpaceshipView : IView<SpaceshipLayout>
{
    public SpaceshipLayout Layout { get; set; }
    public UIPlanetWidget UIWidget { get; set; }

    private SpaceshipModel _model;
    
    public SpaceshipView(SpaceshipModel model)
    {
        _model = model;
    }

    public void Setup()
    {
        OnPositionChanged();
    }

    public void Awake()
    {
        _model.PositionChanged += OnPositionChanged;
        OnPositionChanged();
    }

    public void Update()
    {
        if (UIWidget != null)
        {
            UIWidget.Name.text = _model.GetRating().ToString();
        }
    }

    public void Sleep()
    {
        _model.PositionChanged -= OnPositionChanged;
    }

    public void Destroy()
    {
    }
    
    private void OnPositionChanged()
    {
        if (Layout != null)
        {
            Layout.transform.position = _model.Position;
            Layout.SetBounds(_model.GetBounds());
        }
    }
}
