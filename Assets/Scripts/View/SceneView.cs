﻿using System.Collections.Generic;
using UnityEngine;

public class SceneView : IView<SceneLayout>
{
    public SceneLayout Layout { get; set; }
    private readonly Model _model;
    
    private readonly SpaceshipView _spaceshipView;
    private readonly List<PlanetView> _planetsView;
        
    private readonly CameraView _cameraView;
    
    private readonly Transform _containerPlanets;
    
    public SceneView(Model model)
    {
        _model = model;
        _spaceshipView = new SpaceshipView(_model.Spaceship);
        _planetsView = new List<PlanetView>();
        _cameraView = new CameraView();
        _containerPlanets = new GameObject("[Planets]").transform;
    }

    public void Setup()
    {
        _spaceshipView.Layout = ResourceManager.Create(ResourceManager.SpaceshipPrefab, null).GetComponent<SpaceshipLayout>();
        _spaceshipView.UIWidget = ResourceManager.Create(ResourceManager.SpaceshipWidgetPrefab, Layout.PlanetsController.transform).GetComponent<UIPlanetWidget>();
        _spaceshipView.Setup();
        
        Layout.ButtonZoomIn.onClick.AddListener(ZoomIn);
        Layout.ButtonZoomOut.onClick.AddListener(ZoomOut);
        Layout.ButtonMoveUp.onClick.AddListener(_model.MoveUp);
        Layout.ButtonMoveDown.onClick.AddListener(_model.MoveDown);
        Layout.ButtonMoveRight.onClick.AddListener(_model.MoveRigth);
        Layout.ButtonMoveLeft.onClick.AddListener(_model.MoveLeft);
        
        UpdateZoomButtonEnabled();
    }

    public void Awake()
    {
        _spaceshipView.Awake();
        _cameraView.Awake();
        _model.Spaceship.VisibleSizeChanged += OnVisibleSizeChanged;
        _model.Spaceship.PositionChanged += OnSpaceshipPositionChanged;
        _model.PlanetsChanged += OnPlanetsChanged;
        
        OnVisibleSizeChanged();
        OnSpaceshipPositionChanged();
        OnPlanetsChanged();
    }

    public void Update()
    {
        _spaceshipView.Update();
        _cameraView.Update();  
        
        for (var i = 0; i < _planetsView.Count; ++i)
        {
            _planetsView[i].Update();
        }
    }

    public void Sleep()
    {
        _spaceshipView.Sleep();
        _cameraView.Sleep();    
        _model.Spaceship.VisibleSizeChanged -= OnVisibleSizeChanged;
        _model.Spaceship.PositionChanged -= OnSpaceshipPositionChanged;
        _model.PlanetsChanged -= OnPlanetsChanged;
        Layout.ButtonZoomIn.onClick.RemoveListener(ZoomIn);
        Layout.ButtonZoomOut.onClick.RemoveListener(ZoomOut);
        Layout.ButtonMoveUp.onClick.RemoveListener(_model.MoveUp);
        Layout.ButtonMoveDown.onClick.RemoveListener(_model.MoveDown);
        Layout.ButtonMoveRight.onClick.RemoveListener(_model.MoveRigth);
        Layout.ButtonMoveLeft.onClick.RemoveListener(_model.MoveLeft);

        for (var i = 0; i < _planetsView.Count; ++i)
        {
            _planetsView[i].Sleep();
        }
    }

    public void Destroy()
    {
        _planetsView.Clear();
        _spaceshipView.Destroy();
        _cameraView.Destroy();        
    }

    private void ZoomIn()
    {        
        _model.ZoomIn();
        UpdateZoomButtonEnabled();
        Layout.TextVisibleSize.text = _model.Spaceship.VisibleSize.ToString();
    }

    private void ZoomOut()
    {        
        _model.ZoomOut();
        UpdateZoomButtonEnabled();
        Layout.TextVisibleSize.text = _model.Spaceship.VisibleSize.ToString();
    }

    private void UpdateZoomButtonEnabled()
    {
        Layout.ButtonZoomIn.interactable = (_model.Spaceship.VisibleSize != Settings.DEFAULT_VISIBLE_SIZE);
        Layout.ButtonZoomOut.interactable = (_model.Spaceship.VisibleSize != Settings.MAX_VISIBLE_SIZE);
    }
   
    private void OnPlanetsChanged()
    {
        var nm = _model.VisiblePlanets.Count;
        var nv = _planetsView.Count;
        
        for (var i = nv - 1; i >= 0; --i)
        {
            var contains = false;
            for (var j = nm - 1; j >= 0; --j)
            {
                if ( _model.VisiblePlanets[j] == _planetsView[i].Model)
                {
                    contains = true;
                    break;
                }
            }

            if (!contains)
            {
                ResourceManager.RemoveObject(_planetsView[i].Layout.gameObject);
                ResourceManager.RemoveObject(_planetsView[i].UIWidget.gameObject);
                _planetsView.RemoveAt(i);
            }
        }
        
        nv = _planetsView.Count;
        for (var i = 0; i < nm; ++i)
        {
            var contains = false;
            for (var j = 0; j < nv; ++j)
            {
                if ( _model.VisiblePlanets[i] == _planetsView[j].Model)
                {
                    contains = true;
                    break;
                }
            }
            if (!contains)
            {
                var view = new PlanetView(_model.VisiblePlanets[i]);
                view.Layout = ResourceManager.Create(ResourceManager.PlanetPrefab, _containerPlanets).GetComponent<PlanetLayout>();
                view.UIWidget = ResourceManager.Create(ResourceManager.PlanetWidgetPrefab, Layout.PlanetsController.transform).GetComponent<UIPlanetWidget>();
                view.Setup();
                _planetsView.Add(view);
            }
        }
    }

    private void OnVisibleSizeChanged()
    {
        if (_cameraView != null)
        {
            _cameraView.SetCameraSize(_model.Spaceship.VisibleSize);
        }
    }

    private void OnSpaceshipPositionChanged()
    {
        if (_cameraView != null)
        {
            _cameraView.SetPosition(_model.Spaceship.Position);
        }
    }
}
