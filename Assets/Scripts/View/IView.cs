﻿public interface IView
{
    void Setup();
    void Awake();
    void Update();
    void Sleep();
    void Destroy();
}

public interface IView<T> : IView where T : class
{
    T Layout { get; set; }
}