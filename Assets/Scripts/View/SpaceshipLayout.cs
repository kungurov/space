﻿using UnityEngine;

public class SpaceshipLayout : MonoBehaviour 
{
    public Rect _bounds;
    
    public void SetBounds(Rect bounds)
    {
        _bounds = bounds;
    }

/*    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector3(_bounds.x, _bounds.y, 0), new Vector3(_bounds.x, _bounds.y + _bounds.height, 0));
        Gizmos.DrawLine(new Vector3(_bounds.x, _bounds.y, 0), new Vector3(_bounds.x + _bounds.width, _bounds.y, 0));
        Gizmos.DrawLine(new Vector3(_bounds.x + _bounds.width, _bounds.y, 0), new Vector3(_bounds.x + _bounds.width, _bounds.y + _bounds.height, 0));
        Gizmos.DrawLine(new Vector3(_bounds.x, _bounds.y + _bounds.height, 0), new Vector3(_bounds.x + _bounds.width, _bounds.y + _bounds.height, 0));
    }  
    */  
}
