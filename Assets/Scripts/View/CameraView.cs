﻿using UnityEngine;

public class CameraView : IView
{
    private readonly Transform _cameraTransform;
    
    public CameraView()
    {
        _cameraTransform = Camera.main.transform;
    }

    public void Setup()
    {
    }

    public void Awake()
    {
    }

    public void Update()
    {
    }

    public void Sleep()
    {
    }

    public void Destroy()
    {
    }

    public void SetPosition(Vector2 position)
    {
        _cameraTransform.position = new Vector3(position.x, position.y, _cameraTransform.position.z);
    }

    public void SetCameraSize(int size)
    {
        Camera.main.orthographicSize = size / 2f + 0.5f;
    }
}
