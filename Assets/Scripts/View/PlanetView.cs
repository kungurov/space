﻿using UnityEngine;

public class PlanetView : IView<PlanetLayout>
{
    public PlanetLayout Layout { get; set; }
    public UIPlanetWidget UIWidget { get; set; }

    public PlanetModel Model { get; private set; }

    public PlanetView(PlanetModel model)
    {
        Model = model;
    }

    public void Setup()
    {
        Layout.transform.position = Model.GetPosition();
        Layout.SetBounds(Model.GetBounds());
        UIWidget.Name.text = Model.GetRating().ToString();
    }

    public void Awake()
    {
    }

    public void Update()
    {
        if (UIWidget != null)
        {
            UIWidget.transform.position = Camera.main.WorldToScreenPoint(Model.GetPosition());
        }
    }

    public void Sleep()
    {
    }

    public void Destroy()
    {
    }

}
