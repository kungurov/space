﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public static class ResourceManager
{
    public static string PlanetPrefab = "Prefabs/planet";
    public static string SpaceshipPrefab = "Prefabs/Spaceship";
    public static string PlanetWidgetPrefab = "Prefabs/planet_widget";
    public static string SpaceshipWidgetPrefab = "Prefabs/spaceship_widget";
    
    private static readonly Dictionary<string, Stack<GameObject>> Pool = new Dictionary<string, Stack<GameObject>>();
    
    public static GameObject Create(string key, Transform parent)
    {
        if (!Pool.ContainsKey(key))
        {
            Pool.Add(key, new Stack<GameObject>());
        }
        GameObject obj = null;
        if (Pool[key].Count > 0)
        {
            obj = Pool[key].Pop();
        }
        else
        {
            obj = Object.Instantiate(Resources.Load(key, typeof(GameObject))) as GameObject;
            obj.name = key;
        }
        if (parent != null)
        {
            obj.transform.SetParent(parent, false);
        }
        var component = obj.AddComponent<ResourceComponent>();
        component.Key = key;
        obj.gameObject.SetActive(true);    
        return obj;
    }

    public static void RemoveObject(GameObject obj)
    {
        var component = obj.GetComponent<ResourceComponent>();
        obj.gameObject.SetActive(false);    
        Pool[component.Key].Push(obj);
    }    
}
