﻿using UnityEngine;

public static class Settings
{
    public static int OBJECTS_SAMPLE = 20;
    public static int RATING_MAX = 10000;
    public static int RATING_MIN = 0;
    public static int OBJECTS_PER_QUAD = 10;
    public static Vector2 CELL_SIZE = Vector2.one;
    public static Vector2 PLANET_SIZE = CELL_SIZE / 2f;
    public static float PLANET_CHANCE = 0.3f;
    public static int DEFAULT_VISIBLE_SIZE = 5;
    public static int MAX_VISIBLE_SIZE = 1000;
    public static int START_SPACE_AREA = 50;
}
