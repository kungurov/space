﻿using UnityEngine;

public class ApplicationComponent : MonoBehaviour
{    
    private Model _model;
    private SceneView _view;

    public SceneLayout MainMenuLayout;
    
    public void Awake()
    {
        UnityEngine.Application.targetFrameRate = 60;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Time.fixedDeltaTime = 1000f / 30f;
        Input.simulateMouseWithTouches = false;
        
        _model = new Model();
        _view = new SceneView(_model) { Layout = MainMenuLayout };
        _view.Awake();
        _view.Setup();
    }

    private void Start()
    {
        Screen.SetResolution(Mathf.RoundToInt(1920 * Screen.width / (float) Screen.height), 1920, true, 60);
    }

    public void Update()
    {
        _model.Update();
        _view.Update();
    }

    public void OnDestroy()
    {
        _model.Destroy();
        _view.Destroy();
    }

/*    void OnDrawGizmos()
    {
        _model.Planets.DrawDebug();
    }    
    */
}
